﻿using System;
using System.Drawing;

namespace Ex1M03Uf4YanickGarcia
{
    ///Creación de una figura geometrica base.
    public abstract class FigGeometrica
    {
        public int GetCodi()
        {
            return this.Codi;
        }
        public string GetNom() 
        {
            return this.Nom;    
        }
        public Color GetColor()
        {
            return this.Color;
        }
        public FigGeometrica()
        {
            Codi = 0;
            Nom = "Default";
            Color = Color.White;
        }

        protected int Codi { get; set; }
        protected string Nom { get; set; }
        protected Color Color { get; set; }

        public FigGeometrica(int Codi, string Nom, Color Color)
        {
            this.Codi = Codi;
            this.Nom = Nom;
            this.Color = Color;
        }
        public FigGeometrica(FigGeometrica figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
        }
        public string toString()
        {
            return "Esta figura tiene codigo " + this.Codi + " y nombre " + this.Nom + " y es de color " + this.Color;
        }
        public abstract double Area();
        public override bool Equals(object obj)
        {
            FigGeometrica figuraEqual = (FigGeometrica)obj;
            if (figuraEqual.Codi == this.Codi)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return GetHashCode() * 732;
        }
    }
    ///Creación de la clase rectangulo y metodos para acalcular su area y perimetro.
    class Rectangle : FigGeometrica
    {
        public Rectangle(int Codi, string Nom, Color Color)
            : base(Codi, Nom, Color)
        {
            this.rectBase = 10;
            this.rectHeight = 5;
        }
        public double GetBase()
        {
            return this.rectBase;
        }
        public double GetHeight()
        {
            return this.rectHeight;
        }

        private double rectBase { get; set; }
        private double rectHeight { get; set; }
        public double perimetre(double rectBase, double rectHeight)
        {
            double rectPerimetre = 2 * (rectBase + rectHeight);
            return rectPerimetre;
        }
        public override double Area ()
        {
            double rectArea = rectBase * rectHeight;
            return rectArea;
        }
        public string toString()
        {
            return "\nRECTANGLE\n\nNom: " + GetNom() + "\nCodigo: " + GetCodi() + "\nColor: " + GetColor() + "\nAltura: " + rectHeight + "\nBase: " + rectBase + "\nPerimetre: " + perimetre(rectBase, rectHeight) + "\nArea: " + Area();  
        }

    }
    ///Creación de la clase triangulo y metodos para acalcular su area y perimetro.
    class Triangle : FigGeometrica
    {
        public Triangle(int Codi, string Nom, Color Color)
            : base(Codi, Nom, Color)
        {
            this.trigBase = 6;
            this.trigHeight = 19;
        }
        public double GetBase()
        {
            return trigBase;
        }
        public double GetHeight()
        {
            return trigHeight;
        }

        private double trigBase { get; set; }
        private double trigHeight { get; set; }
        public double perimetre(double trigBase)
        {
            double trigPerimetre = 3 * trigBase;
            return trigPerimetre;
        }
        public override double Area()
        {
            double trigArea = (trigBase * trigHeight) / 2;
            return trigArea;
        }
        public string toString()
        {
            return "\nTRIANGLE:\n\nNom: " + GetNom() + "\nCodigo: " + GetCodi() + "\nColor: " + GetColor() + "\nAltura: " + trigHeight + "\nBase: " + trigBase + "\nPerimetre: " + perimetre(trigBase) + "\nArea: " + Area();
        }
    }
    ///Creación de la clase circulo y metodos para acalcular su area y perimetro.
    class Cercle : FigGeometrica
    {
        private double cercRadi { get; set; }
        public double GetRadi()
        {
            return this.cercRadi;
        }
        public Cercle(int Codi, string Nom, Color Color)
            : base(Codi, Nom, Color)
        {
            this.cercRadi = 13;
        }
        public double perimetre(double cercRadi)
        {
            double cercPerimetre = 2 * Math.PI * cercRadi;
            return cercPerimetre;
        }
        public override double Area()
        {
            double cercArea = Math.PI * (cercRadi * cercRadi);
            return cercArea;
        }
        public string toString()
        {
            return "\nCERCLE:\n\nNom: " + GetNom() + "\nCodigo: " + GetCodi() + "\nColor: " + GetColor() + "\nRadi: " + cercRadi + "\nPerimetre: " + perimetre(cercRadi) + "\nArea: " + Area();
        }
    }
    /// Main para hacer pruebas de las clases.
    class Program
    {
        static void Main(string[] args)
        {
            Cercle circulo = new Cercle(1, "Circulo", Color.Aqua);
            Cercle circulo2 = new Cercle(1, "Circulo", Color.Aqua);
            Triangle triangulo = new Triangle(2,"Triangle",Color.Red);
            //Rectangle rectangulo = new Rectangle(3,"Rectangle",Color.Green);
            Rectangle rectangulo = new Rectangle(1, "Rectangle", Color.Green);



            Console.WriteLine(circulo.toString());
            Console.WriteLine(triangulo.toString());
            Console.WriteLine(rectangulo.toString());

            //Pruebas del equals por codigo
            if (circulo.Equals(rectangulo) == true)
            {
                Console.WriteLine("\nSon iguales!");
            }
            else
            {
                Console.WriteLine("\nSon diferentes!!");
            }

            //Pruebas del Hash con objetos iguales
            //if (circulo.toString().GetHashCode() == circulo2.toString().GetHashCode())
            //{
            //    Console.WriteLine("\nMismo Hash Code!");
            //}
            //else
            //{
            //    Console.WriteLine("\nHash Code diferente!!");
            //}

            //Pruebas del Hash con un objeto diferente
            if (circulo.toString().GetHashCode() == rectangulo.toString().GetHashCode())
            {
                Console.WriteLine("\nMismo Hash Code!");
            }
            else
            {
                Console.WriteLine("\nHash Code diferente!!");
            }
        }
    }
}
